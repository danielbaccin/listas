package br.com.kenui.apps.activity;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.os.Bundle;
import br.com.kenui.apps.adapter.MyPerformanceArrayAdapter;

@SuppressLint("NewApi")
public class ListaComAcaoEmImagensActivity extends ListActivity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		
		String[] values = new String[] { "Android", "iPhone", "WindowsMobile",
			        "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
			        "Linux", "OS/2", "baccinOS" };
		MyPerformanceArrayAdapter adapter = new MyPerformanceArrayAdapter(this, values);
	    setListAdapter(adapter);
	}
	
	

}
