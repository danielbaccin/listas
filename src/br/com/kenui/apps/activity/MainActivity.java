package br.com.kenui.apps.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import br.com.kenui.apps.R;

public class MainActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.action_activity_main.botaoLista1){
			Log.i("agenda", "Botão lista 1 foi pressionado");
			abrirActivity(ListaComStyleActivity.class);
		}else if(item.getItemId() == R.action_activity_main.botaoLista2){
			Log.i("agenda", "Botão lista 2 foi pressionado");
			abrirActivity(ListaSimplesActivity.class);
		}else if(item.getItemId() == R.action_activity_main.botaoLista3){
			abrirActivity(ListaComAcaoEmImagensActivity.class);
		}
		
		
		return super.onOptionsItemSelected(item);
	}

	private void abrirActivity(Class<?> clazz) {
		Intent intent = new Intent();
		intent.setClass(this, clazz);
		startActivity(intent);
		
	}



}
