package br.com.kenui.apps.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.kenui.apps.R;

public class MyPerformanceArrayAdapter extends ArrayAdapter<String>{
	
	private final Activity context;
	private final String[] names;
	
	static class ViewHolder{
		public TextView textView;
		public ImageView imageView;
	}
	
	public MyPerformanceArrayAdapter(Activity context, String[] names) {
		super(context, R.layout.rowlayout, names);
		this.context = context;
		this.names = names;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View rowView = convertView;
		if(rowView == null){
			LayoutInflater inflater =  context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.rowlayout, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.textView = (TextView) rowView.findViewById(R.id.label);
			viewHolder.imageView = (ImageView) rowView.findViewById(R.id.icon);
			rowView.setTag(viewHolder);
		}
		
		ViewHolder holder = (ViewHolder) rowView.getTag();
		String nome = names[position];
		holder.textView.setText(nome);
		if(nome.startsWith("Windows7") || nome.startsWith("iPhone") || nome.startsWith("Linux")){
			holder.imageView.setImageResource(R.drawable.ic_action_cancel);
		}else
			holder.imageView.setImageResource(R.drawable.ic_action_accept);
		
		
		return rowView;
	}
	
	
	
	

}
	