package br.com.kenui.apps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.kenui.apps.R;

public class MySimpleArrayAdapter extends ArrayAdapter<String>{
	
	private Context context;
	private String[] values;
	
	public MySimpleArrayAdapter(Context context, String[] values) {
		super(context, R.layout.rowlayout, values);
		this.context = context;
		this.values = values;
	}

	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.rowlayout, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
		textView.setText(values[position]);
		String s = values[position];
		if(s.startsWith("Windows7") || s.startsWith("iPhone") || s.startsWith("Linux")){
			imageView.setImageResource(R.drawable.ic_action_cancel);
		}else
			imageView.setImageResource(R.drawable.ic_action_accept);
		
		
		return rowView;
	}
	

}
